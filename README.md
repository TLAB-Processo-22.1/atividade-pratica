<h1  align="center"> Git e Github</h1>

<img src="./image/cursoGit.png" alt="">

![Git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white)
![GitHub](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)


## 💻 Sobre o projeto
<em>Git é um sistema de controle de versão distribuído gratuito e de código aberto projetado para lidar com tudo, desde projetos pequenos a muito grandes com velocidade e eficiência.</em>

## Questoẽs teóricas
 ### 1. O que é Git?
  <p>Sistema de controle de versão distribuído</p>

 ### 2. O que é a staging area?
  <p>Local temporário onde são armazenadas todas as alterações que serão adicionadas no próximo commit</p>

 ### 3. O que é o working directory??
  <p>Todos os arquivos que estão sendo trabalhados no momento</p>

 ### 4. O que é um commit?
  <p> É um instantâneo (do inglês, snapshot) das modificações adicionadas na staging area, persistindo-as no repositório local</p>

 ### 5. O que é uma branch?
  <p>ma ramificação do código, apenas um ponteiro que aponta para um commit e tudo anterior a esse commit. O branch padrão é o master</p>

 ### 6. O que é o head no Git?
  <p>É um ponteiro que aponta para algum commit ou alguma branch</p>

 ### 7- O que é um merge?
  <p>Ação de juntar os commits de dois branches</p>

 ### 8. Explique os 4 estados de um arquivo no Git.
  * <p>untracked – Arquivos que não estavam no último commit</p>
  * <p>unmodified – Arquivos não modificados desde o último commit;</p>
  * <p>modified – Arquivos modificados desde o último commit;</p>
  * <p>staged – Arquivos preparados para comitar</p>

### 9. Explique o comando git init.
  <p>Inicializa o repositório</p>

### 10. Explique o comando git add.
  <p> Passa a monitorar o repositório</p>

### 11. Explique o comando git status.
  <p>Mostra o estado do nosso repositório</p>

### 12. Explique o comando git commit.
  <p>Forma de salvar um estado ou versão do código</p>

### 13. Explique o comando git log
  <p>Nos permite vizualisar o histórico de um repositório. É usado quando se precisa encontrar uma versão específica de um projeto ou saber quais alterações vão ser implantadas por meio do merge de uma ramificação de recurso.
  </p>

### 14. Explique o comando git checkout -b.
  <p>
    Comando para criar novos ramos em nosso repositório
  </p>

### 15. Explique o comando git reset e suas três opções
<p>O comando git reset redefine o HEAD atual para o estado especificado</p>

  * <p>soft: que move o HEAD para o commit indicado, mantendo o staging e o work directory inalterados;</p>

  * <p>mixed: altera o HEAD para o commit indicado, altera o staging e mantêm o work directory inalterado</p>

  * <p>hard: altera o HEAD para o commit indicado, alterando o staging e o work directory, fazendo assim com que todas alterações depois daquele commit sejam perdidas</p>

### 16. Explique o comando git revert.
  <p>Reverte alguns commits existentes. Dado um ou mais commits existentes, reverta as alterações introduzidas pelos patches relacionados e grave alguns novos commits que os registrem.</p>

### 17. Explique o comando git clone.
<p>Clona um repositório em um novo diretório</p>

### 18. Explique o comando git push
<p>
  O comando git push é usado para enviar o conteúdo do repositório local para um repositório remoto.</p>

### 19. Explique o comando git pull.
<p>O comando git pull é usado para buscar e baixar conteúdo de repositórios remotos e fazer a atualização imediata ao repositório local para que os conteúdos sejam iguais</p>

### 20. Como ignorar o versionamento de arquivos no Git?
<p>Com o arquivo .gitgnore</p>

### 21. No terralab utilizamos as branches master ou main, develop e staging. Explique o objetivo de cada uma.
<p>No gitflow utilizado no terraLab são utilizados a branch master ou main que é o ramo principal e onde se encontra o código da última versão feita, o ramo develop é onde se encontram os arquivos com as modificações que estão sendo feitas pelos desenvolvedores e a branch staging é onde se encontra todos os arquivos prontos para teste que poderão ser incluídos na próxima release, apenas após passar por todas as rotinas de testes que essa branch é mesclada com o ramo principal</p>

 ## Questoẽs práticas

 #### 1. A essa altura, você já deve ter criado a sua conta do GitLab, não é? Crie um repositório público na sua conta, que vai se chamar Atividade Prática e por fim sincronize esse repositório em sua máquina local.

 #### 2. Dentro do seu reposotorio, crie um arquivo chamado README.md e leia o artigo como fazer um readme.md bonitão e deixe esse README.md abaixo bem bonitão:README.md onde o trainne irá continuamente responder as perguntas em formas de commit.
 
 #### 3.Crie nesse repositório um arquivo que vai se chamar calculadora.js, abra esse arquivo em seu editor de códigos favoritos e adicione o seguinte código:
  
```
  const args = process.argv.slice(2);
  console.log(parseInt(args[0]) + parseInt(args[1]));
```
Descubra o que esse código faz através de pesquisas na internet, também
descubra como executar um código em javascript e dado que o nome do nosso arquivo é calculadora.js e você entendeu o que o código faz, escreva abaixo como executar esse código em seu terminal:

<p>RESPOSTA: O código acima faz a soma de dois números inteiros que são passados por argumento, e para executar ele devemos digitar o comando node calculadora.js argumento1 argumento2. </p>

#### 4. Agora que você já tem um código feito e a resposta aqui, você precisa subir isso para seu repositório. Sem usar git add . descubra como adicionar apenas um arquivo ao seu histórico de commit e adicione calculadora.js a ele.

<p> RESPOSTA: O git pode mapea-lo utilizando o "git add calculadora.js" e depois utiliza-se o git commit -m "Add: calculadora.js".</p>

#### Que tipo de commit o seu README.md deve contar de acordo ao conventional commit. Por fim, faça um push desse commit.

<p>RESPOSTA: Atalho digitando o comando "git commit -am Update: README", já que esse arquivo se encontra sendo mapeado pelo git.
</p>

#### 5.Copie e cole o código abaixo em sua calculadora.js:

´´´

    const soma = () => {

    console.log(parseInt(args[0]) + parseInt(args[1]));

    };

    const args = process.argv.slice(2);

    soma(); ```

#### Descubra o que essa mudança representa em relação ao conventional commit e faça o devido commit dessa mudança.Esse commit cria uma função soma que será executada após o programa capturar os argumentos passados pela linha de comando.

<p>RESPOSTA:  Esse commit cria uma função soma que será executada após o programa capturar os argumentos passados pela linha de comando.</p>

#### 6. João entrou em seu repositório e o deixou da seguinte maneira:

``` const soma = () => {
  console.log(parseInt(args[0]) + parseInt(args[1]));
};

const sub = () => {
  console.log(parseInt(args[0]) - parseInt(args[1]));  
}

const args = process.argv.slice(2);

switch (args[0]) {
    case 'soma':
        soma();
    break;

    case 'sub':
        sub();
    break;

    default:
        console.log('does not support', arg[0]);
}  
```

#### 7. Feito
#### 8. Feito

#### 9. Tentei resolver o revert porém deu esse erro: 

<img src="./image/revert.png" alt="" srcset="">

<p>Tentei procurar na internet porém não achei resposta para o que estava acontecendo, para continuar a atividade eu mesmo alterei o código</p>

#### 10. Descubra como executar esse novo código e que operações ele é capaz de realizar. Deixe sua resposta aqui, e explique o que essas funções javascript fazem. 

<p>RESPOSTA: Para executar esse código devemos digitar o comando "node calculadora.js [nome da função] [num1][num2]", temos nesse código três funções com as funcionalidades de somar, subtrair e dividir, respectivamente. </p>

## Autor

| [<img src="https://gitlab.com/uploads/-/system/user/avatar/12118000/avatar.png?width=400" width=115><br><sub>Lucas Filipe</sub>](https://github.com/camilafernanda) 